<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $role_id
 * @property integer $status
 * @property string $display_name
 * @property string $email
 * @property string $gender
 * @property string $avatar
 * @property string $dob
 * @property string $class
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash'], 'required'],
            [['role_id', 'status'], 'integer'],
            [['gender', 'description'], 'string'],
            [['dob', 'created_at', 'updated_at'], 'safe'],
            [['username', 'display_name'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token', 'avatar'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
            [['class'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'role_id' => 'Role ID',
            'status' => 'Status',
            'display_name' => 'Display Name',
            'email' => 'Email',
            'gender' => 'Gender',
            'avatar' => 'Avatar',
            'dob' => 'Dob',
            'class' => 'Class',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
