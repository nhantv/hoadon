<?php
use yii\helpers\Url;
?>
<div class="breadcrums">
	<div class="container">
		<h1 class="pull-left">Thông Tin Khách Hàng </h1>
		<ul class="pull-right breadcrumb">       
			<li class="active">
				<a href="/">Trang chủ</a>/Thông tin khách hàng
			</li>
        </ul>
	</div>
	<hr>
</div>